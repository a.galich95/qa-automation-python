# #Task Hапишіть программу "Касир в кінотеатрі", яка буде виконувати наступне:
# Попросіть користувача ввести свсвій вік.
# - якщо користувачу менше 7 - вивести "Тобі ж <>! Де твої батьки?"
# - якщо користувачу менше 16 - вивести "Тобі лише <>, а це е фільм для дорослих!"
# - якщо користувачу більше 65 - вивести "Вам <>? Покажіть пенсійне посвідчення!"
# - якщо вік користувача складається з однакових цифр (11, 22, 44 і тд років, всі можливі варіанти!) - вивести "О, вам <>! Який цікавий вік!"
# - у будь-якому іншому випадку - вивести "Незважаючи на те, що вам <>, білетів всеодно нема!"
# Замість <> в кожну відповідь підставте значення віку (цифру) та правильну форму слова рік
# Для будь-якої відповіді форма слова "рік" має відповідати значенню віку користувача.
# Наприклад :
# "Тобі ж 5 років! Де твої батьки?"
# "Вам 81 рік? Покажіть пенсійне посвідчення!"
# "О, вам 33 роки! Який цікавий вік!"
# Зробіть все за допомогою функцій! Для кожної функції пропишіть докстрінг або тайпхінтінг.
# Не забувайте що кожна функція має виконувати тільки одну завдання і про правила написання коду.
# P.S. Для цієї і для всіх наступних домашок пишіть в функціях докстрінги або хінтінг

def determine_correct_spelling_year(age_val):
    """ Determine correct spelling of the ukrainian word year depends on the age value
    Args:
        age_val (int): age value to analyze

    Returns:
        str:
        Correct  of the ukrainian word year (рік, років, роки)
    """
    if (age_val % 10 == 1) and (age_val != 11):
        return 'рік'
    elif (age_val % 10 > 1) and (age_val % 10 < 5) and (age_val != 12) and (age_val != 13) and (age_val != 14):
        return 'роки'
    else:
        return 'років'


def is_interesting_age():
    """ Determine whether age value contains two equal char
    Returns:
        bool: True or False depending on the result

    """
    first_char = age[0]
    count = age.count(first_char)
    length = len(age)
    return count == length and length > 1


def is_valid_age():
    """
    Validation of the input value to have integer value in the range from 1 to 100
    Returns:
        bool: True or False depending on the result

    """
    return age.isnumeric() and 0 < int(age) <= 100


def get_message(age_int):
    """ Return message for the different value of the age
    Args:
        age_int: integer value of the age
    """
    if is_interesting_age():
        print(f'О, вам {age} {correct_spelling_year}! Який цікавий вік!')
    elif age_int < 7:
        print(f'Тобі ж {age} {correct_spelling_year}! Де твої батьки')
    elif age_int <= 16:
        print(f'Тобі лише {age} {correct_spelling_year}, а це е фільм для дорослих!')
    elif age_int >= 65:
        print(f'Вам {age} {correct_spelling_year}? Покажіть пенсійне посвідчення!')
    else:
        print(f'Незважаючи на те, що вам {age} {correct_spelling_year}, білетів всеодно нема!')


age = input('Введіть Ваш вік: ')

if is_valid_age():
    age_int = int(age)
    correct_spelling_year = determine_correct_spelling_year(age_int)
    get_message(age_int)

else:
    print("Помилка валідаціі! Введіть ціле число фбо число у діапазоні від 1 до 100")