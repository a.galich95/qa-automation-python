# Створити клас Vehicle (транспортний засіб):
#
# ні від чого не наслідується
# в ініціалізатор класу (init метод) передати
# producer: str
# model: str
# year: int
# tank_capacity: float # обєм паливного баку
# tank_level: float = 0 # початковий параметр рівня паливного баку дорівнює 0, параметр в аргументах не передавати
# maxspeed: int
# fuel_consumption: float # litres/100km споживання пального
# odometer_value: int = 0 # при сході з конвеєра пробіг нульовий, параметр в аргументах не передавати

class Vehicle:
    def __init__(self, producer, model, year, tank_capacity, max_speed, fuel_consumption):
        self.producer = producer
        self.model = model
        self.year = year
        self.tank_capacity = tank_capacity
        self.tank_level = 0
        self.max_speed = max_speed
        self.fuel_consumption = fuel_consumption
        self.odometer_value = 0

    # визначити метод repr, яким повертати інформаційну стрічку (наповнення на ваш вибір, проте параметри model and year and odometer_value мають бути передані
    def repr(self):
        print(f"Let me show you a new model of the {self.model}. Vehicle's model year is {self.year}. Approximately "
              f"odometer value is  {self.odometer_value}.")

    # написати метод refueling, який не приймає жодного аргумента,
    # заправляє автомобіль на уявній автозаправці до максимума (tank_level = tank_capacity),
    # після чого виводить на екран повідомлення, скільки літрів було заправлено (перша заправка буде повного баку, а в інших випадках величина буде залежати від залишку пального в баку)
    def refueling(self):
        if self.tank_level == 0:
            self.tank_level = self.tank_capacity
            print(f"{self.model} was refueled: {self.tank_capacity} liters")
        else:
            expected_value_to_fuel = self.tank_capacity - self.tank_level
            self.tank_level = self.tank_capacity
            print(f"{self.model} was refueled: {expected_value_to_fuel} liters")

    # написати метод race, який приймає один аргумент (не враховуючи self) - відстань, яку потрібно проїхати, а повертає словник, в якому вказано, скільки авто проїхало,
    # з огляду на заповнення паливного баку перед поїздкою, залишок пального (при малому кілометражі все пальне не використається),
    # та час, за який відбулася дана поїздка, з урахування, що середня швидкість складає 80% від максимальної (витрата пального рівномірна незалежно від швидкості)
    # за результатами роботи метода в атрибуті tank_level екземпляра класа має зберігатися залишок пального після поїздки (зрозуміло, що не менше 0)
    # збільшити на величину поїздки показники odometer_value
    def race(self, distance):
        time = 0
        necessary_amount = distance * self.fuel_consumption / 100
        if self.tank_level >= necessary_amount:
            print("Let's go!!!")
            speed = self.max_speed * 0.8
            time = distance / speed
            self.tank_level -= necessary_amount
            self.odometer_value += distance
        else:
            print(f"You don't have enough fuel. Please carry the necessary amount of fuel or more: {necessary_amount}")

        race_info = {
            "distance": distance,
            "tank_level": self.tank_level,
            "time": time
        }
        print(f"Race info: {race_info}")
        return race_info

    # написати метод lend_fuel, який приймає окрім self ще й other обєкт, в результаті роботи якого паливний бак обєкта, на якому було викликано відповідний метод,
    # наповнюється до максимального рівня за рахунок відповідного зменшення рівня пального у баку дружнього транспортного засобу
    # вивести на екран повідомлення з текстом типу "Дякую, бро, виручив. Ти залив мені *** літрів пального"
    # у випадку, якщо бак першого обєкта повний або у другого обєкта немає пального, вивести повідомлення "Нічого страшного, якось розберуся"
    def lend_fuel(self, other):
        if self.tank_level == self.tank_capacity or other.tank_level == 0:
            print("No problems")
        else:
            necessary_amount = self.tank_capacity - self.tank_level
            available_amount = other.tank_level
            if necessary_amount >= available_amount:
                self.tank_level = other.tank_level
                other.tank_level = 0
            else:
                self.tank_level = self.tank_level + necessary_amount
                other.tank_level -= necessary_amount
            print(f"Thanks guy. You have carried the necessary amount of fuel  ")

    # написати метод get_tank_level, для отримання інформації про залишок пального конкретного інстанса
    def get_tank_level(self):
        return self.tank_level

    # написати метод get_mileage, який поверне значення пробігу odometer_value
    def get_mileage(self):
        return self.odometer_value

    # написати метод eq, який приймає окрім self ще й other обєкт (реалізація магічного методу для оператора порівняння == )
    # даний метод має повернути True у випадку, якщо 2 обєкта, які порівнюються,
    # однакові за показниками року випуску та пробігу (значення відповідних атрибутів однакові, моделі можуть бути різними)
    def __eq__(self, other):
        return (self.year, self.odometer_value) == (other.year, other.odometer_value)


# створіть не менше 2-х обєктів класу, порівняйте їх до інших операцій, заправте їх,
# покатайтесь на них на різну відстань, перевірте пробіг, позичте один в одного пальне, знову порівняйте
print(f"Initialization: ")
print(f"======================Audi 2020: ========================")
audi2020 = Vehicle("Audi", "model2020", 2020, 40, 140, 7.0)
audi2020.repr();
audi2020.refueling()
audi2020.race(10)
print(f"Audi 2020 tank level: {audi2020.get_tank_level()}");
print(f"Odometer value: {audi2020.get_mileage()}")
audi2020.race(1000)
print(f"Audi 2020 tank level: {audi2020.get_tank_level()}");

print(f"======================Audi 2022: ========================")
audi2022 = Vehicle("Audi", "model2022", 2022, 40, 160, 8.0)
audi2022.repr();
audi2022.refueling()

mazda = Vehicle("Mazda", "model2021", 2021, 40, 150, 7.0)
mazda.repr()
mazda.refueling()
print(f"========================Mazda: ========================")
mazda.race(25)
print(f"Odometer value: {mazda.get_mileage()}")
print(f"Mazda 2021 tank level: {audi2020.get_tank_level()}");

mazda.lend_fuel(audi2020)
print(f"Mazda tank level: {mazda.get_tank_level()}")
print(f"Audi tank level: {audi2020.get_tank_level()}")

print(f"Is equal: {mazda.__eq__(audi2020)}")