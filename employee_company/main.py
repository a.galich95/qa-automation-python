from QA_automaton_Alina.employee_company.Company import Company
from QA_automaton_Alina.employee_company.Employee import Employee


def print_salary(employees):
    for employee in employees:
        print(f"{employee.get_name()}: ${employee.get_salary()}")


employee1 = Employee("John", "Doe", "Engineer", 50000)
employee2 = Employee("Jane", "Smith", "Manager", 80000)

print(f"Create a company and add the employees: ")
company = Company("My Corporation")
company.hire_employee(employee1)
company.hire_employee(employee2)

print(f"Initial salaries: ")
print_salary(company.get_employees())

print(f"Give all employees a 50% raise")
company.give_employee_raise(0.5)

print(f"Resulted salaries: ")
print_salary(company.get_employees())
