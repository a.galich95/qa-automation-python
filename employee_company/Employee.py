class Employee:
    """
    Represents an employee with a name, job title, and salary.

    Attributes:
        name (str): The employee's name.
        surname (str): The employee's surname.
        title (str): The employee's job title.
        salary (float): The employee's salary.

    Methods:
        get_name: Returns the employee's name.
        get_surname: Returns the employee's surname.
        get_title: Returns the employee's job title.
        get_salary: Returns the employee's salary.
        set_salary: Sets the employee's salary to a new value.
    """

    def __init__(self, name, surname, title, salary):
        self.name = name
        self.surname = surname
        self.title = title
        self.salary = salary

    def get_name(self):
        """Returns the employee's name."""
        return self.name

    def get_surname(self):
        """Returns the employee's surname."""
        return self.surname

    def get_title(self):
        """Returns the employee's job title."""
        return self.title

    def get_salary(self):
        """Returns the employee's salary."""
        return self.salary

    def set_salary(self, salary):
        """Sets the employee's salary to a new value."""
        self.salary = salary

