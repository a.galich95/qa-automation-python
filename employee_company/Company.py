class Company:
    """
    Represents a company with a name and a list of employees.

    Attributes:
        name (str): The company's name.
        employees (list): A list of Employee objects representing the company's employees.

    Methods:
        get_name: Returns the company's name.
        get_employees: Returns a list of the company's employees.
        hire_employee: Adds a new employee to the company.
        fire_employee: Removes an employee from the company.
        get_total_salary: Calculates the total salary of all employees.
        give_employee_raise: Gives a percentage raise to all employees.
    """

    company_count = 0

    def __init__(self, name, employees=None):
        self.name = name
        self.employees = employees if employees is not None else []
        Company.company_count += 1

    @staticmethod
    def get_company_count():
        return Company.company_count

    def get_name(self):
        """Returns the company's name."""
        return self.name

    def get_employees(self):
        """Returns a list of the company's employees."""
        return self.employees

    def hire_employee(self, employee):
        """Adds a new employee to the company."""
        self.employees.append(employee)

    def fire_employee(self, employee):
        """Removes an employee from the company."""
        self.employees.remove(employee)

    def get_total_salary(self):
        """Calculates the total salary of all employees."""
        total_salary = 0
        for employee in self.employees:
            total_salary += employee.get_salary()
        return total_salary

    def give_employee_raise(self, percent_increase):
        """
        Gives a percentage raise to all employees.

        Args:
            percent_increase (float): The percentage increase to give to all employees.
        """
        for employee in self.employees:
            current_salary = employee.get_salary()
            new_salary = current_salary * (1 + percent_increase)
            employee.set_salary(new_salary)