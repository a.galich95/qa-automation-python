def determine_correct_spelling_year(age_val):
    """ Determine correct spelling of the ukrainian word year depends on the age value
    Args:
        age_val (int): age value to analyze

    Returns:
        str:
        Correct  of the ukrainian word year (рік, років, роки)
    """
    if (age_val % 10 == 1) and (age_val != 11):
        return 'рік'
    elif (age_val % 10 > 1) and (age_val % 10 < 5) and (age_val != 12) and (age_val != 13) and (age_val != 14):
        return 'роки'
    else:
        return 'років'


def is_interesting_age(age):
    """ Determine whether age value contains two equal char
    Args:
        age: age string
    Returns:
        bool: True or False depending on the result

    """
    first_char = age[0]
    count = age.count(first_char)
    length = len(age)
    return count == length and length > 1


def is_valid_age(age):
    """Validation of the input value to have integer value in the range from 1 to 100
    Args:
        age: age string
    Returns:
        bool: True or False depending on the result

    """
    return age.isnumeric() and 0 < int(age) <= 100


def get_message(age_int, correct_spelling_year, age):
    """ Return message for the different value of the age
    Args:
        age: age string
        correct_spelling_year: correct spelling year
        age_int: integer value of the age
    """
    if is_interesting_age(age):
        print(f'О, вам {age} {correct_spelling_year}! Який цікавий вік!')
    elif age_int < 7:
        print(f'Тобі ж {age} {correct_spelling_year}! Де твої батьки')
    elif age_int <= 16:
        print(f'Тобі лише {age} {correct_spelling_year}, а це е фільм для дорослих!')
    elif age_int >= 65:
        print(f'Вам {age} {correct_spelling_year}? Покажіть пенсійне посвідчення!')
    else:
        print(f'Незважаючи на те, що вам {age} {correct_spelling_year}, білетів всеодно нема!')