import functions_lib

age = input('Введіть Ваш вік: ')

if functions_lib.is_valid_age(age):
    age_int = int(age)
    correct_spelling_year = functions_lib.determine_correct_spelling_year(age_int)
    functions_lib.get_message(age_int, correct_spelling_year, age)

else:
    print("Помилка валідаціі! Введіть ціле число фбо число у діапазоні від 1 до 100")