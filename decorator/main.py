def skip(condition, reason=''):
    def decorator(func):
        def wrapper(*args, **kwargs):
            if condition:
                print(f"Skipped {func.__name__}: {reason}")
            else:
                return func(*args, **kwargs)

        return wrapper

    return decorator


@skip(condition=True, reason='because of JIRA-123 bug')
def add(x, y):
    return x + y


result = add(2, 3)
print(result)
