class Pizza:
    """
    Represents a pizza with a crust, sauce, and toppings.
    """

    def __init__(self):
        """
        Initialize the pizza with default values for crust, sauce, and toppings.
        """
        self.crust = ""
        self.sauce = ""
        self.toppings = []

    def __str__(self):
        """
        Returns a string representation of the pizza.
        """
        return f"{self.crust} pizza with {self.sauce} sauce and {', '.join(self.toppings)}"


class PizzaBuilder:
    """
    A builder for creating custom pizzas.
    """

    def __init__(self):
        """
        Initialize the pizza builder with an empty pizza object.
        """
        self.pizza = Pizza()

    def set_crust(self, crust):
        """
        Set the crust of the pizza.

        Args:
            crust (str): The type of crust to use.

        Returns:
            self: This PizzaBuilder instance, for method chaining.
        """
        self.pizza.crust = crust
        return self

    def set_sauce(self, sauce):
        """
        Set the sauce of the pizza.

        Args:
            sauce (str): The type of sauce to use.

        Returns:
            self: This PizzaBuilder instance, for method chaining.
        """
        self.pizza.sauce = sauce
        return self

    def add_topping(self, topping):
        """
        Add a topping to the pizza.

        Args:
            topping (str): The name of the topping to add.

        Returns:
            self: This PizzaBuilder instance, for method chaining.
        """
        self.pizza.toppings.append(topping)
        return self

    def build(self):
        """
        Build and return the completed pizza object.

        Returns:
            Pizza: The completed pizza object.
        """
        return self.pizza


# Client code
print(f"GoF pattern Builder ")
print(f"Cook pizza: ")
pizza_builder = PizzaBuilder()
pizza = pizza_builder.set_crust("thin").set_sauce("tomato").add_topping("cheese").add_topping("pepperoni").build()
print(pizza)
