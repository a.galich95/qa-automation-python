# Розробити функцію, котра приймає колекцію та обʼєкт функції, що приймає один аргумент.
# Певернути колекцію, кожен член якої є перетвореним членом вхідної колекції.
# Нотатка. Обʼєкт функції, яку передаємо вказує на функцію, котра приймає один аргумент.
# Не користуватися функціями map чи filter!!!

def process_collection(process, data):
    return [process(element) for element in data]

def process(element):
    return element * 3

initial_list = [1, 2, 3, 4]
print(f"Initial list: {initial_list}")
print(f"Processed list: {process_collection(process, initial_list)}")