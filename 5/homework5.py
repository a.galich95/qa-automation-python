#Task1 Напишіть функцію, що приймає один аргумент. Функція має вивести на екран тип цього аргумента (для визначення типу використайте type)

def return_variable_type(arg):
    print('Тип аргумента: ', type(arg))
variable = 'Hello world'
return_variable_type(variable)

#Task2 Напишіть функцію, що приймає один аргумент будь-якого типу та повертає цей аргумент, перетворений на float.
# Якщо перетворити не вдається функція має повернути 0 (флоатовий нуль).

def convert_to_float(arg):
        try:
            return float(arg)
        except:
            return 0.0
variable = input('Введіть данні для конвертації: ')
print('Перетворені данні', convert_to_float(variable))

#Task3 Напишіть функцію, що приймає два аргументи. Функція повинна якщо аргументи відносяться до числових типів - повернути різницю цих аргументів,
# якщо обидва аргументи це строки - обʼєднати в одну строку та повернути
# якщо перший строка, а другий ні - повернути dict де ключ це перший аргумент, а значення - другий
# у будь-якому іншому випадку повернути кортеж з цих аргументів

def process_agruments(first_arg, second_arg):
    if first_arg.isnumeric() and second_arg.isnumeric():
        return int(first_arg) - int(second_arg)
    elif isinstance(first_arg, str) and isinstance(second_arg, str) and not second_arg.isdigit():
        return first_arg + second_arg
    elif isinstance(first_arg, str) and second_arg.isdigit():
        return dict({first_arg: second_arg})
    else:
        tuple((first_arg, second_arg))


input_value_first = input('Введiть перше значення: ')
input_value_second = input('Введiть друге значення: ')

print("Результат обробки даних", process_agruments(input_value_first, input_value_second))