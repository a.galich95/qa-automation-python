# Task 1 Зформуйте строку, яка містить певну інформацію про символ в відомому слові. Наприклад "The [номер символу] symbol in [тут слово] is '[символ з відповідним
# порядковим номером]'". Слово та номер отримайте за допомогою input() або скористайтеся константою.
# Наприклад (слово - "Python" а номер символу 3) - "The 3 symbol in "Python" is 't' ".

print('Task 1')
word = input('Введіть слово: ')
char_num = input('Введіть номер символа(відлік починаєм з 0): ')

i = int(char_num)
if char_num.isnumeric() and i < len(word):
    counter = 0;
    while True:
        if counter == i:
            print(f"The {char_num} symbol in '{word}' is '{word[i]}'")
            break
        counter += 1
    print('Дякую за увагу')
else:
    print("Помилка валідації")

#  Task 2 Вести з консолі строку зі слів (або скористайтеся константою). Напишіть код, який визначить кількість кількість слів, в цих даних.

print('Task 2')
my_str = input('Введіть строку зі слів: ')
result = my_str.split()
print('Кількість слів: ', len(result))

# Task 3 Існує ліст з різними даними, наприклад lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишіть механізм, який сформує новий list (наприклад lst2), який би містив всі числові змінні, які є в lst1. Майте на увазі,
# що данні в lst1 не є статичними можуть змінюватись від запуску до запуску.

print('Task 3')
lst_initial = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
print('Initial list: ', lst_initial)
lst_int = list()
for x in lst_initial:
  if isinstance(x, int) and not isinstance(x, bool):
      lst_int.append(x)
print('List with Int:', lst_int)




