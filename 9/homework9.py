# Завдання №1
#
# Розробити клас Людина. Людина має:
#
# Ім'я
# Прізвище
# Вік (атрибут але ж змінний)
# Стать
# Люди можуть:
#
# Їсти
# Спати
# Говорити
# Ходити
# Стояти
# Лежати
# Також ми хочемо контролювати популяцію людства.
# Змінювати популяцію можемо в __init__.
# Треба сказати, що доступ до статичних полів класу з __init__ не можу іти
# через НазваКласу.статичий_атрибут, позаяк ми не може бачити імені класу.
# Але натомість ми можемо сказати self.__class__.static_attribute.

import datetime

class Human:
    population = 0
    def __init__(self, name, surname, sex, birth_day = datetime.date.today()):
        self.name = name
        self.surname = surname
        self.sex = sex
        self.__birth_day = birth_day
        self.__class__.population +=1
    @property
    def age(self):
        return (datetime.date.today() - self.__birth_day).days // 365
    def eat (self, food):
        print(f"{self.surname} {self.name} is eating {food}. Sex is {self.sex}. Age is {alina.age} years old.")
    def sleep (self):
        print(f"{self.surname} {self.name} is sleeping now")
    def speak (self, quikly):
        print(f"{self.surname} {self.name} is speaking {quikly}")
    def walk (self, dog):
        print(f"{self.surname} {self.name} is walking with the {dog}")
    def stand (self, floor):
        print(f"{self.surname} {self.name} is standing on the {floor}")
    def lie (self, sofa):
        print(f"{self.surname} {self.name} is lying on the {sofa}. Age is {anna.age} years old.")


print(f"Initial population: {Human.population}")
alina = Human("Alina", "Galich", "girl", birth_day=datetime.date(1998, 2, 3))
anna = Human("Anna", "Lesovaya", "girl", birth_day=datetime.date(2001, 5, 8))
alina.eat("borsch")
alina.sleep()
alina.speak("quikly")
anna.stand("floor")
anna.lie("sofa")
anna.walk("cat")
print(f"Current population: {Human.population}")




