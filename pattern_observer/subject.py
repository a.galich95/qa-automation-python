class Subject:
    """
    Represents the subject being observed.
    """

    def __init__(self):
        """
        Initialize the subject with an empty list of observers.
        """
        self.observers = []

    def attach(self, observer):
        """
        Attach an observer.py to the subject.

        Args:
            observer (Observer): The observer.py to attach.
        """
        self.observers.append(observer)

    def detach(self, observer):
        """
        Detach an observer.py from the subject.

        Args:
            observer (Observer): The observer.py to detach.
        """
        self.observers.remove(observer)

    def notify(self, message):
        """
        Notify all observers of a message.

        Args:
            message (str): The message to send to observers.
        """
        for observer in self.observers:
            observer.update(message)

