class Observer:
    """
    Represents an observer that is notified of changes to a subject.
    """

    def update(self, message):
        """
        Update the observer with a message.

        Args:
            message (str): The message sent by the subject.
        """
        pass
