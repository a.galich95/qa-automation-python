from pattern_observer.concrete_subject import ConcreteSubject
from pattern_observer.сoncrete_observer import ConcreteObserver

print(f"Gof pattern Observer ")
subject = ConcreteSubject()
observer1 = ConcreteObserver("Observer 1")
observer2 = ConcreteObserver("Observer 2")

subject.attach(observer1)
subject.attach(observer2)

subject.set_state("new state")

subject.detach(observer1)

subject.set_state("updated state")
