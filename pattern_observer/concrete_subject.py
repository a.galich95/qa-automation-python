from pattern_observer.subject import Subject


class ConcreteSubject(Subject):
    """
    Represents a concrete subject that is observed by observers.
    """

    def __init__(self):
        """
        Initialize the concrete subject with an empty list of observers.
        """
        super().__init__()
        self.state = ""

    def set_state(self, state):
        """
        Set the state of the concrete subject.

        Args:
            state (str): The new state of the subject.
        """
        self.state = state
        self.notify(state)
