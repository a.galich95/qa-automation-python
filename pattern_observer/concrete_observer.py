from pattern_observer.observer import Observer


class ConcreteObserver(Observer):
    """
    Represents a concrete observer.py that is notified of changes to a subject.
    """

    def __init__(self, name):
        """
        Initialize the concrete observer.py with a name.

        Args:
            name (str): The name of the observer.py.
        """
        self.name = name

    def update(self, message):
        """
        Update the observer.py with a message.

        Args:
            message (str): The message sent by the subject.
        """
        print(f"{self.name} received message: {message}")

