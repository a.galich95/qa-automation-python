from abc import ABC, abstractmethod


class Payable(ABC):
    @abstractmethod
    def get_payment_amount(self):
        pass
