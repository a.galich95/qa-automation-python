import datetime
from abc import ABC

from oop_principles_employee.Payable import Payable
from oop_principles_employee.Person import Person


class Employee(Person, Payable, ABC):
    def __init__(self, employee_id, first_name, last_name, birth_day, gender, address, title, phone, email_address,
                 department=None, leave=None):
        super().__init__(first_name, last_name, birth_day, gender, address)
        self._employee_id = employee_id
        self._title = title
        self._phone = phone
        self._email_address = email_address
        self._department = department
        self._leave = leave

    @property
    def age(self):
        return (datetime.date.today() - self.birth_day).days // 365

    @property
    def first_name(self):
        return super().first_name

    @property
    def last_name(self):
        return super().last_name



