from oop_principles_employee.Employee import Employee


class HourlyEmployee(Employee):
    def __init__(self, employee_id, first_name, last_name, birth_day, gender, address, title,
                 phone, email_address, hourly_rate, hours_worked):
        super().__init__(employee_id, first_name, last_name, birth_day, gender, address, title, phone, email_address)
        self._hourly_rate = hourly_rate
        self._hours_worked = hours_worked

    def get_payment_amount(self):
        return self._hourly_rate * self._hours_worked
