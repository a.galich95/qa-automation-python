class Person:
    def __init__(self, first_name, last_name, birth_day, gender, address):
        self._first_name = first_name
        self._last_name = last_name
        self._birth_day = birth_day
        self._gender = gender
        self._address = address

    @property
    def first_name(self):
        return self._first_name

    @first_name.setter
    def first_name(self, first_name):
        self._first_name = first_name

    @property
    def last_name(self):
        return self._last_name

    @last_name.setter
    def last_name(self, last_name):
        self._last_name = last_name

    @property
    def birth_date(self):
        return self._birth_day

    @birth_date.setter
    def birth_day(self, birth_day):
        self._birth_day = birth_day

    @property
    def gender(self):
        return self._gender

    @gender.setter
    def gender(self, gender):
        self._gender = gender

    @property
    def address(self):
        return self._address

    @address.setter
    def address(self, address):
        self._address = address