class Address:
    def __init__(self, street, city, state, postal_code, country):
        self._street = street
        self._city = city
        self._state = state
        self._zip_code = postal_code
        self._country = country

    @property
    def street(self):
        return self._street

    @street.setter
    def street(self, street):
        self._street = street

    @property
    def city(self):
        return self._city

    @city.setter
    def city(self, city):
        self._city = city

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, state):
        self._state = state

    @property
    def zip_code(self):
        return self._zip_code

    @zip_code.setter
    def zip_code(self, zip_code):
        self._zip_code = zip_code

    @property
    def country(self):
        return self._country

    @country.setter
    def country(self, country):
        self._country = country

