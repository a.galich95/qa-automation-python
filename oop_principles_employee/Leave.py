class Leave:
    def __init__(self, id, type, description, status, date_to, date_from):
        self._id = id
        self._type = type
        self._description = description
        self._status = status
        self._date_to = date_to
        self._date_from = date_from
