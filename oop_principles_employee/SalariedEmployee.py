from oop_principles_employee.Employee import Employee


class SalariedEmployee(Employee):
    def __init__(self, employee_id, first_name, last_name, birth_day, gender, address, title,
                 phone, email_address, salary):
        super().__init__(employee_id, first_name, last_name, birth_day, gender, address, title,
                         phone, email_address)
        self._salary = salary

    def get_payment_amount(self):
        return self._salary
