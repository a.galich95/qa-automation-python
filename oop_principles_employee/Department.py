class Department:
    def __init__(self, id, name, manager, employees=None):
        self._id = id
        self._name = name
        self._manager = manager
        self._employees = employees if employees is not None else []

    def add_employee(self, employee):
        self._employees.append(employee)

    def remove_employee(self, employee):
        self._employees.remove(employee)

    def get_employee_count(self):
        return len(self.employees)

    def add_manager(self, manager):
        self._manager = manager

    @property
    def employees(self):
        return self._employees

    @property
    def name(self):
        return self._name

