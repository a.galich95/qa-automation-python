import datetime

from oop_principles_employee.Address import Address
from oop_principles_employee.Department import Department
from oop_principles_employee.HourlyEmployee import HourlyEmployee
from oop_principles_employee.SalariedEmployee import SalariedEmployee

address1 = Address("123 Main St", "Anytown", "12345", "Kv", "UA")
address2 = Address("456 Shevchenko Ave", "Othertown", "12345", "Kh", "UA")

hourly_employee = HourlyEmployee(1, "John", "Doe", datetime.date(1998, 2, 3), "M", address1, "Junior Software Developer",
                                 "0943-44355534", "john.doe@example.com", 200.0, 20)
salaried_employee = SalariedEmployee(2, "Jane", "Smith", datetime.date(1990, 2, 3), "F", address2,
                                     "Middle Software Developer", "0943-899857567", "jane.smith@example.com", 5000.0)
manager = SalariedEmployee(2, "Sofia", "Smith", datetime.date(1988, 5, 4), "F", address2, "Junior Software Developer",
                           "0943-890999997", "sofia.smith@example.com", 60000.0)

# Create a department
department = Department(1, "Developers", manager)

# Add employees to the department
department.add_employee(hourly_employee)
department.add_employee(salaried_employee)

# Generate a report on employee salaries
print(f"Salary report for {department.name} department:\n")
for employee in department.employees:
    print(f"{employee.first_name}: ${employee.get_payment_amount()}")
