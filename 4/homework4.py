# Task2 Є два довільних числа які відповідають за мінімальну і максимальну ціну.
# Є Dict з назвами магазинів і цінами: { "cilpio": 47.999, "a_studio" 42.999, "momo": 49.999, "main-service": 37.245,
# "buy.ua": 38.324, "my-store": 37.166, "the_partner": 38.988, "sto": 37.720, "rozetka": 38.003}.
# Напишіть код, який знайде і виведе на екран назви магазинів, ціни яких попадають в діапазон між мінімальною і максимальною ціною.
# Наприклад:
# lower_limit = 35.9
# upper_limit = 37.339
# > match: "my-store", "main-service"

print('Task 2')
dict = {
    "cilpio": 47.999,
    "a_studio": 42.999,
    "momo": 49.999,
    "main-service": 37.245,
    "buy.ua": 38.324,
    "my-store": 37.166,
    "the_partner": 38.988,
    "sto": 37.720,
    "rozetka": 38.003
}

lower_limit = float(38.65)
upper_limit = float(47.986)
print('Назви магазинів, ціни яких попадають в діапазон між мінімальною і максимальною ціною: ')

for key in dict:
    if lower_limit < dict.get(key) < upper_limit:
        print(key)

#Task1 Дана довільна строка. Напишіть код, який знайде в ній і віведе на екран кількість слів, які містять дві голосні літери підряд.

print('Task 1')
my_str = input('Введіть строку зі слів: ')
given_tuple = ('a', 'e', 'i', 'o', 'u')
result = my_str.split()
count = 0
for word in result:
    for i in range(len(word)):
        if word[i] in given_tuple and i + 1 < len(word):
            if word[i + 1] in given_tuple:
                count = count + 1
                break

print('Кількість слів: ', count)